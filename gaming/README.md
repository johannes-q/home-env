# Introduction

Powerful gaming computer for programming, writing, browsing, PC Games and for emulation
with high performance requirements, like Nintendo Gamecube, Wii etc.

Connected to tv.

Computer runs windows for PC game, emulator and LaunchBox compatibility.

Suppports bluetooth.

Uses windows with WSL for great software compatibility.

Wireless Switch Pro Controller is the joypad of choice.

Fish is the shell of choice. Might be Rash in the future.

Racket is the programming language of choice.
The expressive power of the language helps me to stay
focused and keeps my motivation up.

I use VSCode and Emacs.

# Instructions

Username has to be "Johannes Qvarford".

* Type Win+Powershell, right click run as administrator
* Copy+Paste the content of the install file.
* Restart
* Copy+Paste the content of the install2 file.

# How to connect