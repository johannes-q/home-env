
## References

* Read https://docs.microsoft.com/en-us/windows/wsl/install-win10
* Read https://itnext.io/using-wsl-2-to-develop-java-application-on-windows-8aac1123c59b?gi=adf3bcc4fa53
* Read https://docs.microsoft.com/en-us/windows/terminal/get-started
* Read https://techcommunity.microsoft.com/t5/windows-dev-appconsult/running-wsl-gui-apps-on-windows-10/ba-p/1493242
* Read https://www.hanselman.com/blog/the-easy-way-how-to-ssh-into-bash-and-wsl2-on-windows-10-from-an-external-machine
* Read https://github.com/PowerShell/Win32-OpenSSH/wiki/Logging-Facilities
* Read https://www.cnet.com/how-to/automatically-log-in-to-your-windows-10-pc/

For some reason the personal startup directory changed from Startup to STARTUP-.
Had to restore it:
https://answers.microsoft.com/en-us/windows/forum/windows_7-performance/shortcuts-in-the-startup-folder-not-running-during/646845d6-8c10-4456-b6ef-b99f4a03aa10
